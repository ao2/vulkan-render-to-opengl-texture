/*
 * vulkan-render-to-texture-glDrawVkImageNV - Render to an OpenGL texture from Vulkan with glDrawVkImageNV
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */
#ifndef VULKAN_RENDER_TO_OPENGL_TEXTURE_H
#define VULKAN_RENDER_TO_OPENGL_TEXTURE_H

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>

#ifdef __cplusplus
extern "C" {
#endif

extern PFNGLDRAWVKIMAGENVPROC fn_glDrawVkImageNV;
extern PFNGLWAITVKSEMAPHORENVPROC fn_glWaitVkSemaphoreNV;
extern PFNGLSIGNALVKSEMAPHORENVPROC fn_glSignalVkSemaphoreNV;

extern int width;
extern int height;

void init (void);

void deinit (void);

void draw_screen (GLuint target_texture, int width, int height);

void error_callback (int error, const char *description);

void GLAPIENTRY debug_callback (GLenum source, GLenum type, GLuint id,
    GLenum severity, GLsizei length,
    const GLchar * message, const void *userParam);

#ifdef __cplusplus
}
#endif

#endif
