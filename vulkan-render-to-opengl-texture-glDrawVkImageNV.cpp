/*
 * vulkan-render-to-texture-glDrawVkImageNV - Render to an OpenGL texture from Vulkan with glDrawVkImageNV
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>
#include <stdlib.h>

#include "vulkan-render-to-opengl-texture-glDrawVkImageNV.h"
#include "renderheadless.h"

PFNGLDRAWVKIMAGENVPROC fn_glDrawVkImageNV;
PFNGLWAITVKSEMAPHORENVPROC fn_glWaitVkSemaphoreNV;
PFNGLSIGNALVKSEMAPHORENVPROC fn_glSignalVkSemaphoreNV;

int width = 1024;
int height = 1024;

#define SCREENSHOT_MAX_FILENAME 256
static char screenshot_filename[SCREENSHOT_MAX_FILENAME];
static GLubyte *pixels = NULL;
static const size_t format_nchannels = 3;

static unsigned int nframes = 0;

VulkanExample* vkExInstance = NULL;
VkImage vkImage = NULL;
VkSemaphore vkRenderDone = NULL;
VkSemaphore vkPresentDone = NULL;

/*
 * Take screenshot with glReadPixels and save to a file in PPM format.
 *
 * - filename: file path to save to, without extension
 * - width: screen width in pixels
 * - height: screen height in pixels
 * - pixels: intermediate buffer to avoid repeated mallocs across multiple calls.
 */
static void
screenshot_ppm (const char *filename, int image_width,
    int image_height, size_t nchannels, GLubyte ** image_pixels)
{
  int i, j;
  size_t cur;
  FILE *f = fopen (filename, "w");
  fprintf (f, "P3\n%d %d\n%d\n", image_width, image_height, 255);
  glReadPixels (0, 0, image_width, image_height, GL_RGB, GL_UNSIGNED_BYTE,
      *image_pixels);
  for (i = 0; i < image_height; i++) {
    for (j = 0; j < image_width; j++) {
      cur = nchannels * (i * image_width + j);
      fprintf (f, "%3d %3d %3d ", (*image_pixels)[cur],
          (*image_pixels)[cur + 1], (*image_pixels)[cur + 2]);
    }
    fprintf (f, "\n");
  }
  fclose (f);
}

void
init (void)
{
  pixels = (GLubyte *)malloc(format_nchannels * sizeof (GLubyte) * width * height);
  if (pixels == NULL) {
      fprintf(stderr, "allocating pixels failed\n");
      exit(EXIT_FAILURE);
  }
  vkExInstance = vulkan_example_init(width, height);

  fprintf (stdout, "Info: %s\n", "init completed");
}

void
deinit (void)
{
  vulkan_example_deinit(vkExInstance);
  free (pixels);
}

static void
draw_scene (void)
{
  auto vkData = vulkan_example_render(vkExInstance);
  vkRenderDone = std::get<0>(vkData);
  vkPresentDone = std::get<1>(vkData);
  vkImage = std::get<2>(vkData);
}

void
draw_screen (GLuint target_texture, int width, int height)
{
  //fprintf (stdout, "Info: Render start: %d\n", nframes);
  draw_scene ();

  /* Offscreen rendering framebuffer. */
  static GLuint scene_fbo;
  glGenFramebuffers (1, &scene_fbo);
  glBindFramebuffer (GL_FRAMEBUFFER, scene_fbo);

  glBindTexture (GL_TEXTURE_2D, target_texture);
  glFramebufferTexture2D (GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
      target_texture, 0);

  GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers (1, DrawBuffers);

  /* Sanity check. */
  if (glCheckFramebufferStatus (GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    fprintf (stderr, "glCheckFramebufferStatus() failed.");
    return;
  }

  glClearColor(0.7f, 0.0f, 0.7f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  fn_glWaitVkSemaphoreNV((GLuint64)vkRenderDone);

  //fprintf (stdout, "Info: %s\n", "GPU Copy start");

  fn_glDrawVkImageNV((GLuint64)vkImage, 0.0f,
                  0.0f, 0.0f, width, height,  // dest coords
                  0.0f,                    // z??
                  0.0f, 0.0f, 1.0f, 1.0f); // source s, t coords

  glFlush ();

#if 1
    snprintf (screenshot_filename, SCREENSHOT_MAX_FILENAME,
              "frame-%05d.ppm", nframes);
    screenshot_ppm (screenshot_filename, width, height, format_nchannels, &pixels);
#endif

  fn_glSignalVkSemaphoreNV((GLuint64)vkPresentDone);
  //fprintf (stdout, "Info: %s\n", "GPU Copy end");

  nframes++;
}

void
error_callback (int error, const char *description)
{
  (void) error;
  fprintf (stderr, "Error: %s\n", description);
}

void GLAPIENTRY
debug_callback (GLenum source, GLenum type, GLuint id,
    GLenum severity, GLsizei length,
    const GLchar * message, const void *userParam)
{
  (void) source;
  (void) id;
  (void) length;
  (void) userParam;
  fprintf (stderr,
      "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
      (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity,
      message);
}
