# Collection of examples about how to render to OpenGL textures from Vulkan

The examples can be built with the following commands:

```
$ meson build/
$ ninja -C build/
```

## test-glDrawVkImageNV

```
./build/test-glDrawVkImageNV
```

**NOTE**: The `glDrawVkImageNV` implementation on NVIDIA proprietary drivers seems to be broken when used from EGL with the X11 backend. This workaround seems to avoid the crash:

```
env -u DISPLAY ./build/test-glDrawVkImageNV
```
