#pragma once

#include <vulkan/vulkan.h>

#include <tuple>

class VulkanExample;

VulkanExample* vulkan_example_init(int32_t width, int32_t height);

/* the first semaphore is renderDone, the second semaphore is presentDone */
std::tuple<VkSemaphore, VkSemaphore, VkImage> vulkan_example_render(VulkanExample* vkExInstance);

VkDevice vulkan_example_get_device(VulkanExample* vkExInstance);

void vulkan_example_deinit(VulkanExample* vkExInstance);
