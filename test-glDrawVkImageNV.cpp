/*
 * test-glDrawVkImageNV - Render to an OpenGL texture from Vulkan with glDrawVkImageNV
 *
 * Copyright 2021, Collabora Ltd
 * Author: Antonio Ospite <antonio.ospite@collabora.com>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <EGL/egl.h>

#include "vulkan-render-to-opengl-texture-glDrawVkImageNV.h"

int main (void)
{
  init ();

  EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

  if (!eglInitialize(display, NULL, NULL)) {
    switch(eglGetError()){
      case EGL_BAD_DISPLAY:
        fprintf(stderr, "Failed to initialize EGL Display: EGL_BAD_DISPLAY\n");
        break;
      case EGL_NOT_INITIALIZED:
        fprintf(stderr, "Failed to initialize EGL Display: EGL_NOT_INITIALIZED\n");
        break;
      default:
        fprintf(stderr, "Failed to initialize EGL Display: unknown erro\n");
        break;
    }

    exit (EXIT_FAILURE);
  }

  eglBindAPI(EGL_OPENGL_API);

  EGLint configAttribs[11] = {
    EGL_BLUE_SIZE, 8,
    EGL_GREEN_SIZE, 8,
    EGL_RED_SIZE, 8,
    EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
    EGL_CONFORMANT, EGL_OPENGL_BIT,
    EGL_NONE
  };

  EGLint numConfigs = 0;
  EGLConfig eglConfig = nullptr;
  if(!eglChooseConfig(display, configAttribs, &eglConfig, 1, &numConfigs)){
      switch(eglGetError()){
        case EGL_BAD_DISPLAY:
          fprintf(stderr, "Failed to configure EGL Display: EGL_BAD_DISPLAY\n");
          break;
        case EGL_BAD_ATTRIBUTE:
          fprintf(stderr, "Failed to configure EGL Display: EGL_BAD_ATTRIBUTE\n");
          break;
        case EGL_NOT_INITIALIZED:
          fprintf(stderr, "Failed to configure EGL Display: EGL_NOT_INITIALIZED\n");
          break;
        case EGL_BAD_PARAMETER:
          fprintf(stderr, "Failed to configure EGL Display: EGL_BAD_PARAMETER\n");
          break;
        default:
          fprintf(stderr, "Failed to configure EGL Display: unknown error\n");
          break;
      }
    exit (EXIT_FAILURE);
  }

  EGLContext context = eglCreateContext(display, eglConfig, EGL_NO_CONTEXT, NULL);
  if (context == EGL_NO_CONTEXT) {
    fprintf(stderr, "Failed to initialize EGL context: EGL_NO_CONTEXT\n");
    exit (EXIT_FAILURE);
  }

  eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, context);

  const GLubyte* vendor = glGetString(GL_VENDOR);
  const GLubyte* renderer = glGetString(GL_RENDERER);
  const GLubyte* version = glGetString(GL_VERSION);
  const GLubyte* glslVer = glGetString(GL_SHADING_LANGUAGE_VERSION);
  fprintf(stdout, "OpenGL Context: %s : %s (%s - GLSL: %s)\n", vendor, renderer, version, glslVer);

  fprintf(stdout, "Info: Checking for required extensions:\n");
  const char* NV_DRAW_VULKAN_EXT_STR = "GL_NV_draw_vulkan_image";
  int numberOfExtensions;
  glGetIntegerv(GL_NUM_EXTENSIONS, &numberOfExtensions);

  for (int i = 0; i < numberOfExtensions; i++) {
    const char * extStr = (const char*)glGetStringi(GL_EXTENSIONS, i);
    if (strcmp(extStr, NV_DRAW_VULKAN_EXT_STR) == 0) {
      fprintf(stdout, "Info: \t%s supported\n", NV_DRAW_VULKAN_EXT_STR);
      fn_glDrawVkImageNV = (PFNGLDRAWVKIMAGENVPROC)eglGetProcAddress("glDrawVkImageNV");
      fn_glWaitVkSemaphoreNV = (PFNGLWAITVKSEMAPHORENVPROC)eglGetProcAddress("glWaitVkSemaphoreNV");
      fn_glSignalVkSemaphoreNV = (PFNGLSIGNALVKSEMAPHORENVPROC)eglGetProcAddress("glSignalVkSemaphoreNV");
    }
  }

  if (fn_glDrawVkImageNV == NULL) {
    fprintf(stderr, "Failed to find the %s extension!\n", NV_DRAW_VULKAN_EXT_STR);
    exit(EXIT_FAILURE);
  }

  glEnable (GL_DEBUG_OUTPUT);
  glDebugMessageCallback (debug_callback, NULL);

  GLuint scene_texture;
  glGenTextures (1, &scene_texture);
  glBindTexture (GL_TEXTURE_2D, scene_texture);
  glTexStorage2D (GL_TEXTURE_2D, 1, GL_RGB8, width, height);
  glBindTexture (GL_TEXTURE_2D, 0);

  uint32_t frame_count = 10;
  while (frame_count-- > 0) {
    glViewport (0, 0, width, height);

    // Render scene into the target texture
    draw_screen (scene_texture, width, height);
  }

  deinit ();

  exit (EXIT_SUCCESS);
}
